Source: skrooge
Section: kde
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Pino Toscano <pino@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               dh-python,
               gettext,
               kf6-extra-cmake-modules (>= 1.7.0),
               plasma-activities-dev,
               kf6-karchive-dev,
               kf6-kcolorscheme-dev,
               kf6-kcompletion-dev,
               kf6-kconfig-dev,
               kf6-kconfigwidgets-dev,
               kf6-kcoreaddons-dev,
               kf6-kdoctools-dev,
               kf6-kguiaddons-dev,
               kf6-ki18n-dev,
               kf6-kiconthemes-dev,
               kf6-kitemviews-dev,
               kf6-kjobwidgets-dev,
               kf6-kio-dev,
               kf6-knewstuff-dev,
               kf6-knotifications-dev,
               kf6-knotifyconfig-dev,
               kf6-kparts-dev,
               kf6-krunner-dev,
               kf6-kstatusnotifieritem-dev,
               kf6-ktexttemplate-dev,
               kf6-kwallet-dev,
               kf6-kwidgetsaddons-dev,
               kf6-kwindowsystem-dev,
               kf6-kxmlgui-dev,
               libofx-dev,
               libqca-qt6-2-dev,
               libsqlcipher-dev,
               pkgconf,
               pkg-kde-tools-neon,
               python3:any,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-svg-dev,
               qt6-tools-dev,
               qt6-webengine-dev,
               shared-mime-info
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://skrooge.org/
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/skrooge.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/skrooge

Package: skrooge
Architecture: any
Depends: libqca-qt6-2,
         qt6-base,
         qt6-declarative,
         qt6-webengine,
         skrooge-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: poppler-utils
Suggests: aqbanking-tools (>= 5.6.10)
Description: personal finance manager for KDE
 Skrooge allows you to manage your personal finances. It is intended to be used
 by individuals who want to keep track of their incomes, expenses and
 investments. Its philosophy is to stay simple and intuitive.
 .
 Here is the list of Skrooge main features:
  * QIF, CSV, KMyMoney, Skrooge,  import/export
  * OFX, QFX, GnuCash, Grisbi, HomeBank import
  * Advanced Graphical Reports
  * Several tabs to help you organize your work
  * Infinite undo/redo
  * Instant filtering on operations and reports
  * Infinite categories levels
  * Mass update of operations
  * Scheduled operations
  * Track refund of your expenses
  * Automatically process operations based on search conditions
  * Multi currencies
  * Dashboard

Package: skrooge-common
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Skrooge architecture independent files
 Skrooge allows you to manage your personal finances. It is intended to be used
 by individuals who want to keep track of their incomes, expenses and
 investments. Its philosophy is to stay simple and intuitive.
 .
 This package contains architecture independent files needed for Skrooge to run
 properly. It also provides Skrooge documentation. Therefore, unless you
 have 'skrooge' package installed, you will hardly find this package useful.
